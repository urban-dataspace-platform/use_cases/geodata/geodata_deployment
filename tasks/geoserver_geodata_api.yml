#############################
#### CREATE GEODATA API   ####
#############################

- name: Get Admin credentials
  k8s_info:
    namespace: '{{ inv_idm.ns_name | lower }}'
    kubeconfig: '{{ k8s_config }}'
    context: '{{ k8s_context }}'
    kind: Secret
    name: '{{ inv_idm.keycloak.k8s_secret_name }}'
  register: admin_credentials

- name: Store IDM Credentials
  set_fact:
    ADMIN_USERNAME: "{{ admin_credentials.resources[0].data.MASTER_USERNAME | b64decode }}"
    ADMIN_PASSWORD: "{{ admin_credentials.resources[0].data.MASTER_PASSWORD | b64decode }}"
    cacheable: yes

- name: Login to keycloak
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/realms/master/protocol/openid-connect/token"
    return_content: yes
    body_format: form-urlencoded
    body:
        grant_type: "password"
        client_id: "admin-cli"
        username: "{{ ADMIN_USERNAME }}"
        password: "{{ ADMIN_PASSWORD }}"
    status_code: 200
  register: keycloak_login

- name: Get clients
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ inv_idm.keycloak.realm_name }}/clients"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: clients

- name: "Get keycloak client API Access ID."
  set_fact:
    client_id_api_access: "{{ clients.json | json_query(query) }}"
  vars:
    query: "[?clientId== '{{ default.IDM_CLIENT.API_ACCESS }}'].id | [0]"

- name: "Get keycloak client Gravitee ID."
  set_fact:
    client_id_gravitee: "{{ clients.json | json_query(query) }}"
  vars:
    query: "[?clientId== '{{ default.IDM_CLIENT.GRAVITEE }}'].id | [0]"

- name: Get Client Secret API Access
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ inv_idm.keycloak.realm_name }}/clients/{{ client_id_api_access }}/client-secret"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: keycloak_client_api_access_secret_response

- name: Get Client Secret Gravitee
  uri:
    method: GET
    url: "https://idm.{{ DOMAIN }}/auth/admin/realms/{{ inv_idm.keycloak.realm_name }}/clients/{{ client_id_gravitee }}/client-secret"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ keycloak_login.json.access_token }}
    status_code: 200
  register: keycloak_client_gravitee_secret_response

- name: "Set IDM client secret API Access."
  set_fact:
    IDM_CLIENT_SECRET_API_ACCESS: "{{ keycloak_client_api_access_secret_response.json.value }}"
    cacheable: yes

- name: "Set IDM client secret Gravitee."
  set_fact:
    IDM_CLIENT_SECRET_GRAVITEE: "{{ keycloak_client_gravitee_secret_response.json.value }}"
    cacheable: yes

- name: Login into APIM management API using IDM provider
  uri:
    method: POST
    url: "https://idm.{{ DOMAIN }}/auth/realms/{{ inv_idm.keycloak.realm_name }}/protocol/openid-connect/token"
    return_content: yes
    body_format: form-urlencoded
    body:
    - [ grant_type, "password" ]
    - [ username, "{{ ADMIN_USERNAME }}" ]
    - [ password, "{{ ADMIN_PASSWORD }}" ]
    - [ client_id, "{{ default.IDM_CLIENT.GRAVITEE }}" ]
    - [ client_secret, "{{ IDM_CLIENT_SECRET_GRAVITEE }}" ]
    headers:
      Accept: application/json
    status_code: 200
  register: user

- name: Print login response
  debug:
    var: user

- name: Exchange token at APIM
  uri:
    method: POST
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/auth/oauth2/keycloak/exchange?token={{ user.json.access_token }}"
    return_content: yes
    headers:
      Accept: application/json
    status_code: 200
  register: new_token


######## Change from here


- name: Create Geodata API Entry
  uri:
    method: POST
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/apis/import"
    return_content: yes
    body_format: json
    body: |
      {
        "proxy": {
          "endpoints": [
            {
              "name":"default",
              "target":"http://txlgeoserver-geoserver.{{ k8s_namespace }}.svc.cluster.local:80",
              "inherit":true
            }
          ],
          "context_path":"/gateway/geoserver"
        },
        "pages":[],
        "plans":[],
        "tags":[],
        "name":"Geoserver",
        "version":"1.0",
        "description":"Geoserver"
      }
    headers:
      Accept: application/json
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 200,400
  register: geodataAPI

- name: Print Geodata API already exists
  debug:
    msg: "Failed. Geodata API already existing?"
  when: geodataAPI.status == 400

- name: Migrate to Design Studio
  uri:
    method: POST
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/apis/{{ geodataAPI.json.id }}/_migrate"
    return_content: yes
    body_format: json
    body: ""
    headers:
      Accept: application/json
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 200
  when: geodataAPI.status != 400

- name: Update to final Values
  uri:
    method: PUT
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/apis/{{ geodataAPI.json.id }}"
    return_content: yes
    body_format: json
    body: "{{ lookup('template', 'templates/geodata/geoserver.json') }}"
    headers:
      Accept: application/json
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 200
  when: geodataAPI.status != 400

- name: Create Subscription Plan based on keycloak-auth
  uri:
    method: POST
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/apis/{{ geodataAPI.json.id }}/plans"
    return_content: yes
    body_format: json
    body: "{{ lookup('template', 'templates/geodata/create_plan_oauth_keycloak.json.j2') }}"
    headers:
      Accept: application/json
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 201
  register: fiwarePlan
  when: geodataAPI.status != 400

- name: Start the API
  uri:
    method: POST
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/apis/{{ geodataAPI.json.id }}?action=START"
    return_content: yes
    headers:
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 204
  when: geodataAPI.status != 400

- name: Get current applications
  uri:
    method: GET
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/applications"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 200
  register: applications

- name: Applications
  ansible.builtin.debug:
    msg: "{{ applications }}"

- name: Get portal application id
  set_fact:
    portal_app_id: "{{ applications.json | json_query(query) }}"
  vars:
    query: "[?name == '{{ inv_am.apim.portal_application }}' && owner.displayName == '{{ inv_idm.platform.admin_first_name }} {{ inv_idm.platform.admin_surname }}'].id | [0]"

- name: Subscribe platform application to Orion
  uri:
    method: POST
    url: "https://apim.{{ DOMAIN }}/management/organizations/DEFAULT/environments/DEFAULT/apis/{{ geodataAPI.json.id }}/subscriptions?application={{ portal_app_id }}&plan={{ fiwarePlan.json.id }}"
    return_content: yes
    headers:
      Accept: application/json
      Authorization: Bearer {{ new_token.json.token }}
    status_code: 201,400
  register: portal_geodata_subscription
  when: geodataAPI.status != 400

- name: Check if already existing.
  debug:
    msg: "Failed. Subscription may already exist."
  when: geodataAPI.status != 400 and portal_geodata_subscription.status == 400

- name: Print subscription response
  debug:
    var: portal_geodata_subscription